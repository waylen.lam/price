@extends('car.layouts')

@section('title')
{{ __('Car Management') }}
@endsection

@section('content')


@forelse($cars as $car)

<div class="car">
    <div class="name">{{ $car->name }}</div>
    <div class="color">{{ $car->color }}</div>
    <div class="price">{{ $car->formattedPrice() }}</div>
</div>

@empty

No Cars.

@endforelse

<form action="{{ route('car.manage.add') }}" method="POST">
@csrf
Name: <input type="text" name="name" /><br>
Color: <input type="text" name="color" /><br>
Price: <input type="text" name="price" /><br>
<input type="submit" />
</form>

@endsection
