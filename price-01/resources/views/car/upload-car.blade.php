@extends('car.layouts')

@section('title', 'Upload car')

@section('content')

<form action="{{ route('car.upload') }}" method="POST" enctype="multipart/form-data">
@csrf

{{ __('File:') }}<input type="file" name="file" />

<input type="submit" />

</form>
{{ __('this is upload car', ['name' => '<b>Alex</b>']) }}

@endsection
