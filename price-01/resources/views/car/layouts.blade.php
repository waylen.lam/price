<html>

    <head>
        <title>@yield('title')</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>

    <body>
        {{ __('Hello World!') }}

        @component('nav')
            car
        @endcomponent

        @yield('content')

        @auth
        <form method="post" action="{{ route('logout') }}">
        @csrf
        <input type="submit" value="Logout" />
        </form>
        @endauth

        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
