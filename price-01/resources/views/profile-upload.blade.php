@extends('car.layouts')

@section('title')
{{ __('Profile Upload ')}}
@endsection

@section('content')
<form method="POST" action="{{ route('profile-upload') }}" enctype="multipart/form-data">
    @csrf

    <input type="file" name="profile">

    <input type="submit">
</form>
@endsection
