<?php

namespace Price\Helpers;

class RandomNumberGenerator {
    private $rand;

    public function __construct() {
        $this->rand = rand();
    }

    public function getRandom() {
        return $this->rand;
    }
}
