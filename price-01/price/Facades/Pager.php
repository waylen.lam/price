<?php

namespace Price\Facades;

use Illuminate\Support\Facades\Facade;

class Pager extends Facade {
    protected static function getFacadeAccessor() {
        return 'pager';
    }
}
