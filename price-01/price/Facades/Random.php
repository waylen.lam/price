<?php

namespace Price\Facades;

use Illuminate\Support\Facades\Facade;
use Price\Helpers\RandomNumberGenerator;

class Random extends Facade {
    protected static function getFacadeAccessor() {
        return RandomNumberGenerator::class;
    }
}
