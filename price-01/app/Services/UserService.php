<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService {
    public function __construct(UserRepository $userRepository ){
        $this->userRepository = $userRepository;
    }

    public function createUser($username, $password) {
        return $this->userRepository->createUser(
            $username,
            Hash::make($password)
        );
    }

    public function getUserByUsername($username) {
        return $this->userRepository->getUserByUsername(
            $username
        );
    }

    public function getUserByFacebook($fbUser) {
        $dbUser = $this->userRepository->getUserByFacebookId($fbUser->id);
        if ($dbUser) {
            return $dbUser;
        }

        $dbUser = $this->getUserByUsername($fbUser->email);
        if ($dbUser) {
            return $dbUser;
        }

        return null;
    }

    public function linkUserWithFacebookId($userId, $facebookId) {
        return $this->userRepository->linkUserWithFacebookId(
            $userId, $facebookId
        );
    }
}
