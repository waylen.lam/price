<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\CarRepository;

class CarService {
    public function __construct(CarRepository $carRepository) {
        $this->carRepository = $carRepository;
    }

    public function getAllCars() {
        return $this->carRepository->getAll();
    }

    public function getLatestCar() {
        return $this->carRepository->getLatest();
    }

    public function addCar($name, $color, $price) {
        if ($color === 'blue') {
            $price *= 0.9;
        }
        $this->carRepository->add($name, $color, $price);
    }
}
