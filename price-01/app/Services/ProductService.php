<?php

namespace App\Services;

use App\Exceptions\OutOfStockException;
use App\Repositories\IProductRepository;

class ProductService {
    public function __construct(IProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    public function getAll() {
        return $this->productRepository->getAll();
    }

    public function addToCart($productId) {
        $stocks = $this->productRepository->stockOfWarehouses($productId);

        foreach ($stocks as $stock) {
            if ($stock > 0) {
                return true;
            }
        }

        throw new OutOfStockException();
    }
}
