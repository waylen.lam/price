<?php

namespace App\Repositories;

use App\Product;
use Illuminate\Support\Facades\DB;
use App\Repositories\IProductRepository;

class ProductRepository implements IProductRepository {

    public function addProduct(Product $product) {
        DB::table('products')->insert([
            'date' => date('Y-m-d', $product->unformattedDate),
            'expiry_date' => $product->expiry_date == 0 ? null : date('Y-m-d', $product->expiry_date),
            'bulk' => json_encode($product->bulk),
            'price' => $product->unformattedPrice
        ]);
    }

    public function getAll() {
        return DB::table('products')->get();
    }

    public function removeAllExpired() {
        DB::table('products')
            ->where('expiry_date', '<=', $this->getCurrentTime())
            ->whereNotNull('expiry_date')
            ->delete();
    }

    public function getCurrentTime() {
        return now();
    }

}
