<?php

namespace App\Repositories;

use App\Product;
use Illuminate\Support\Facades\Cache;
use App\Repositories\IProductRepository;

class CachedProductRepository implements IProductRepository {
    public function __construct(IProductRepository $source) {
        $this->source = $source;
    }

    public function addProduct(Product $product) {
        $this->source->addProduct($product);
        $this->flush();
    }

    public function getAll() {
        return Cache::rememberForever('products.getAll', function() {
            return $this->source->getAll();
        });
    }

    public function removeAllExpired() {
        $this->source->removeAllExpired();
        $this->flush();
    }

    protected function flush() {
        Cache::forget('products.getAll');
    }
}
