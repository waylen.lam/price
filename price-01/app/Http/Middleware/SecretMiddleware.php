<?php

namespace App\Http\Middleware;

use Closure;

class SecretMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $version = 1)
    {
        $response = $next($request);

        $response->header('NW-VERSION', $version);

        return $response;
    }
}
