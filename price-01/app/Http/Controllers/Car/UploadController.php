<?php

namespace App\Http\Controllers\Car;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    //
    public function show() {
        return view('car.upload-car');
    }
//
    public function upload(Request $request) {
        $path = $request->file('file')->store('images', 's3');

        $url = Storage::disk('s3')->temporaryUrl($path, now()->addMinutes(5));

        $request->session()->flash('status', 'File Uploaded');

        return redirect()->route('car.upload-form');
    }
}
