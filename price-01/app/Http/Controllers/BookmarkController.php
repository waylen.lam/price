<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    //
    public function show(Request $request, $order) {
        return response($request->query('order'), 200);
    }
}
