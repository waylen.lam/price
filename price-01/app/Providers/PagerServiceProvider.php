<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Price\Helpers\Pager;

class PagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('pager', Pager::class);
    }
}
