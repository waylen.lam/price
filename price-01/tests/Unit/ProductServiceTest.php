<?php

namespace Tests\Unit;

use Mockery;
use Exception;
use Tests\TestCase;
use App\Services\ProductService;
use App\Exceptions\OutOfStockException;
use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductServiceTest extends TestCase
{
    public function testProductServiceReturnInStock()
    {
        $productRepository = Mockery::mock(ProductRepository::class);
        $productRepository->shouldReceive('stockOfWarehouses')
            ->with(1)
            ->andReturn([
                0, 0, 10
            ]);

        $productService = new ProductService($productRepository);

        $result = $productService->addToCart(1);

        $this->assertEquals(true, $result);
    }

    public function testProductServiceThrowOnOutOfStock()
    {
        $this->expectException(OutOfStockException::class);

        $productRepository = Mockery::mock(ProductRepository::class);
        $productRepository->shouldReceive('stockOfWarehouses')
            ->with(1)
            ->andReturn([
                0, 0, 0
            ]);

        $productService = new ProductService($productRepository);
        $productService->addToCart(1);
    }

    public function testProductList() {
        $productRepository = Mockery::mock(ProductRepository::class);
        $productRepository->shouldReceive('getAll')
            ->andReturn([]);

        $productService = new ProductService($productRepository);
        $result = $productService->getAll();
        $this->assertEquals([], $result);
    }
}
