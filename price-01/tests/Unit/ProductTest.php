<?php

namespace Tests\Unit;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    protected function setUp() {
        parent::setUp();

        $this->product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'price' => 1000,
            'expiry_date' => null,
            'bulk' => [
                [
                    'quantity' => 0,
                    'discount' => 0
                ],
                [
                    'quantity' => 10,
                    'discount' => 0.1
                ],
                [
                    'quantity' => 20,
                    'discount' => 0.2
                ],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDateFormat()
    {
        // Act
        $result = $this->product->formattedDate();

        // Assert
        $this->assertEquals("2018/1/1", $result);
    }

    public function testPriceFormat()
    {
        // Act
        $result = $this->product->formattedPrice();

        // Assert
        $this->assertEquals('HKD1,000.00', $result);
    }

    public function testBulkPurchaseZero() {
        $result = $this->product->getDiscountOfQuantity(0);

        $this->assertEquals(0, $result);
    }

    public function testBulkPurchaseZeroNearBoundary() {
        $result = $this->product->getDiscountOfQuantity(9);

        $this->assertEquals(0, $result);
    }

    public function testBulkPurchaseTier1() {
        $result = $this->product->getDiscountOfQuantity(10);

        $this->assertEquals(0.1, $result);
    }

    public function testBulkPurchaseTier2() {
        $result = $this->product->getDiscountOfQuantity(20);

        $this->assertEquals(0.2, $result);
    }

    public function testBulkPurchaseTier2Boundary() {
        $result = $this->product->getDiscountOfQuantity(99999999);

        $this->assertEquals(0.2, $result);
    }

    public function testBulkPurchaseOverrideSetup() {
        $this->product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'expiry_date' => null,
            'price' => 1000,
            'bulk' => []
        ]);

        $result = $this->product->getDiscountOfQuantity(99999999);

        $this->assertEquals(0, $result);
    }

    public function testBulkPurchaseTier1Custom() {
        $this->product->setBulk([
            [
                'quantity' => 0,
                'discount' => 0
            ],
            [
                'quantity' => 20,
                'discount' => 0.1
            ],
            [
                'quantity' => 40,
                'discount' => 0.3
            ],
        ]);

        $result = $this->product->getDiscountOfQuantity(20);

        $this->assertEquals(0.1, $result);
    }

}
