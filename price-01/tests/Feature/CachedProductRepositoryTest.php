<?php

namespace Tests\Feature;

use Mockery;
use App\Product;
use Tests\TestCase;
use App\Repositories\IProductRepository;
use Illuminate\Foundation\Testing\WithFaker;
use App\Repositories\CachedProductRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CachedProductRepositoryTest extends TestCase
{
    public function testCached()
    {
        $sourceRepository = \Mockery::mock(IProductRepository::class);
        $sourceRepository->shouldReceive('getAll')
            ->times(1)
            ->andReturn([]);

        $cachedRepository = new CachedProductRepository($sourceRepository);

        $cachedRepository->getAll();
        $cachedRepository->getAll();
    }

    public function testCachedFlush()
    {
        $sourceRepository = \Mockery::mock(IProductRepository::class);
        $sourceRepository->shouldReceive('getAll')
            ->times(2)
            ->andReturn([]);
        $sourceRepository->shouldReceive('addProduct');

        $mockProduct = \Mockery::mock(Product::class);

        $cachedRepository = new CachedProductRepository($sourceRepository);

        $cachedRepository->getAll();
        $cachedRepository->getAll();

        $cachedRepository->addProduct($mockProduct);

        $cachedRepository->getAll();
        $cachedRepository->getAll();
    }
}
