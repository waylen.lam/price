<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use App\Mail\GreetingMail;
use App\Console\Commands\SendEmails;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendMailsFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() {
        parent::setUp();
        $this->repository = new UserRepository();
        $this->repository->createUser('test@price.com.hk', '');
    }

    public function testSendEmailsCommand()
    {
        $send = \Mockery::mock();
        $send->shouldReceive('send')
            ->with(GreetingMail::class)
            ->andReturn(true);

        Mail::shouldReceive('to')
            ->andReturn($send);

        $command = new SendEmails($this->repository);
        $command->handle();
    }
}
